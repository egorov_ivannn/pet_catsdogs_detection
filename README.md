# pet_catsdogs_detection

The project is a toy test task for Junior CV engineer at Neurus

The task was to create a network which is able to classify cat/dog on the given image and detect its face with a cnn model.

**pet_neurus.ipynb** includes transfer learning approach of solving the task.
I chose ResNet50 model for the training.
The approach achieved meanIOU of 0.9 and roc-auc/accuracy of 0.99. Such a high accuracy was achieved because the data is a subset of imagenet, which weights were used during training.

All results are mIoU 90%, classification accuracy 99%, 12ms, 2708 train, 677 valid.

**pet_neurus_cnn.ipynb** includes my implementation of a vanila cnn model.
For the vanila cnn I chose VGG16 model to recreate and modified it a little bit.
This approach didn't get high results as expected.

All results are mIoU 70%, classification accuracy 74%, 6ms, 2708 train, 677 valid.


